package com.gadware.andoid.e_market.repository;

import androidx.lifecycle.MutableLiveData;

import com.gadware.andoid.e_market.models.ProductInfo;
import com.gadware.andoid.e_market.models.ProductResponse;
import com.gadware.andoid.e_market.rest.Api;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Repository {
    private Api api;
    public void callApi( MutableLiveData<List<ProductInfo>> liveData){
        Call<ProductResponse>call=api.getProductList();
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    liveData.postValue(response.body().getProductInfoList());
                }else{
                    liveData.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {

            }
        });
    }
}
