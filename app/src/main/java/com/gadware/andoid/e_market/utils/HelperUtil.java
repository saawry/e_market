package com.gadware.andoid.e_market.utils;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.gadware.andoid.e_market.R;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class HelperUtil {
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String getTime(String time){
        String pattern = "HH:mm:ss.SSSZ";
        String pattern2 = "hh:mm a";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.US);
        Date date = null;
        try {
            date = simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        simpleDateFormat = new SimpleDateFormat(pattern2, Locale.US);
        return simpleDateFormat.format(date);
    }

    public static Bitmap loadBitmapFromView(View view, int width, int height) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);


        return returnedBitmap;
    }

    public static void animateView(View view,View target, AppCompatImageView imgCpy, Bitmap b) {
        imgCpy.setImageBitmap(b);
        imgCpy.setVisibility(View.VISIBLE);
        int[] u = new int[2];

        target.getLocationInWindow(u);
        imgCpy.setLeft(view.getLeft());
        imgCpy.setTop(view.getTop());

        AnimatorSet animSetXY = new AnimatorSet();
        ObjectAnimator y = ObjectAnimator.ofFloat(imgCpy, "translationY", imgCpy.getTop(), u[1]);
        ObjectAnimator x = ObjectAnimator.ofFloat(imgCpy, "translationX", imgCpy.getLeft(), u[0]);
        ObjectAnimator sy = ObjectAnimator.ofFloat(imgCpy, "scaleY", 0.5f, 0.1f);
        ObjectAnimator sx = ObjectAnimator.ofFloat(imgCpy, "scaleX", 0.5f, 0.1f);
        animSetXY.playTogether(x, y, sx, sy);
        animSetXY.setDuration(650);
        animSetXY.start();
    }

    public static void runAnimationAgain(Context context, RecyclerView recycler) {

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.lay_anim_down_to_up);

        recycler.setLayoutAnimation(controller);
        //adapter.notifyDataSetChanged();
        recycler.scheduleLayoutAnimation();

    }
}
