package com.gadware.andoid.e_market.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.gadware.andoid.e_market.R;
import com.gadware.andoid.e_market.databinding.CardProductBinding;
import com.gadware.andoid.e_market.models.ProductInfo;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private final List< ProductInfo>  itemList;
    private final Context context;
    QuantityChangeInterface quantityChangeInterface;

    public ProductsAdapter(Context context, List<ProductInfo> itemList, QuantityChangeInterface quantityChangeInterface) {
        this.itemList = itemList;
        this.context = context;
        this.quantityChangeInterface = quantityChangeInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardProductBinding binding = CardProductBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(binding);

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductInfo model = itemList.get(position);

        holder.binding.productName.setText(model.getName());
        holder.binding.price.setText("" + model.getPrice());
        holder.binding.bill.setText("" + model.getPrice());

        Picasso.get().load(model.getImageUrl()).placeholder(context.getDrawable(R.drawable.ic_baseline_image)).into(holder.binding.productImage);

        holder.binding.addBtn.setOnClickListener(v -> {
            String count = holder.binding.count.getText().toString();
            holder.binding.count.setText(String.valueOf(Integer.parseInt(count) + 1));
            holder.binding.bill.setText(String.valueOf(Integer.parseInt(holder.binding.price.getText().toString()) + model.getPrice()));
            quantityChangeInterface.onQuantityChange(position, Integer.parseInt(count), model.getPrice(),holder.binding.cardView);
        });

        holder.binding.minusBtn.setOnClickListener(v -> {
            String count = holder.binding.count.getText().toString();
            if (Integer.parseInt(count) > 0) {
                holder.binding.count.setText(String.valueOf(Integer.parseInt(count) - 1));
                holder.binding.bill.setText(String.valueOf(Integer.parseInt(holder.binding.price.getText().toString()) - model.getPrice()));
                quantityChangeInterface.onQuantityChange(position, Integer.parseInt(count), model.getPrice(),holder.binding.cardView);
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardProductBinding binding;

        public ViewHolder(CardProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface QuantityChangeInterface {
        void onQuantityChange(long id, int count, long price, CardView card);
    }
}