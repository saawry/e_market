package com.gadware.andoid.e_market.models;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ProductResponse {
    private List <ProductInfo> productInfoList;
}
