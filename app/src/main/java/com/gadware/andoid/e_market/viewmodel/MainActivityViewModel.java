package com.gadware.andoid.e_market.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gadware.andoid.e_market.models.ProductInfo;
import com.gadware.andoid.e_market.repository.Repository;
import com.gadware.andoid.e_market.rest.Api;

import java.lang.annotation.Inherited;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import lombok.Data;

@Data
@HiltViewModel
public class MainActivityViewModel extends ViewModel {

    MutableLiveData<List<ProductInfo>> liveData;

    @Inject
    Api api;

    @Inject
    public MainActivityViewModel(){
        liveData=new MutableLiveData<>();
    }


    public void makeApiCall(){
        Repository repository=new Repository(api);
        repository.callApi(liveData);
    }
}
