package com.gadware.andoid.e_market.models;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class OrderModel {
    private long id;
    private String name;
    private long price;
    private int quantity;
    private String imageUrl;
}
