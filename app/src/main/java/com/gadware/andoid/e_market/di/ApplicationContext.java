package com.gadware.andoid.e_market.di;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class ApplicationContext extends Application {
}
