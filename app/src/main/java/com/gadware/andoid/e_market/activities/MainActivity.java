package com.gadware.andoid.e_market.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.gadware.andoid.e_market.databinding.ActivityMainBinding;
import com.gadware.andoid.e_market.models.ProductInfo;
import com.gadware.andoid.e_market.viewmodel.MainActivityViewModel;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = binding.toolbarLayout;
        toolBarLayout.setTitle(getTitle());


        InitRecycler();
        InitViewModel();

    }

    private void InitRecycler() {

    }

    private void InitViewModel() {
        MainActivityViewModel mainActivityViewModel=new ViewModelProvider(this).get(MainActivityViewModel.class);
        mainActivityViewModel.getLiveData().observe(this, new Observer<List<ProductInfo>>() {
            @Override
            public void onChanged(List<ProductInfo> productInfos) {
                if (productInfos==null){
                    Toast.makeText(MainActivity.this, "Something went wrong !", Toast.LENGTH_SHORT).show();
                }else{
                    //add to adapter
                }
            }
        });
        mainActivityViewModel.makeApiCall();
    }


}