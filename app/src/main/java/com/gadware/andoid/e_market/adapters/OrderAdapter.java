package com.gadware.andoid.e_market.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.gadware.andoid.e_market.R;
import com.gadware.andoid.e_market.databinding.CardProductBinding;
import com.gadware.andoid.e_market.models.OrderModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private final List<OrderModel> itemList;
    private final Context context;


    public OrderAdapter(Context context, List< OrderModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardProductBinding binding = CardProductBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ViewHolder(binding);

    }

    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderModel model = itemList.get(position);

        assert model != null;
        holder.binding.productName.setText(model.getName());
        holder.binding.price.setText("" + model.getPrice());
        holder.binding.bill.setText("" + model.getPrice());

        Picasso.get().load(model.getImageUrl()).placeholder(context.getDrawable(R.drawable.ic_baseline_image)).into(holder.binding.productImage);

        holder.binding.minusBtn.setVisibility(View.GONE);
        holder.binding.addBtn.setVisibility(View.GONE);

    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardProductBinding binding;

        public ViewHolder(CardProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}