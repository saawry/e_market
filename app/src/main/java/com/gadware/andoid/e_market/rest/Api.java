package com.gadware.andoid.e_market.rest;


import com.gadware.andoid.e_market.models.ProductInfo;
import com.gadware.andoid.e_market.models.ProductResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {

    @GET("storeInfo")
    Call<ResponseBody> getStoreInfo();

    @GET("products")
    Call<ProductResponse> getProductList();

    @FormUrlEncoded
    @POST("order")
    Call<ResponseBody> orderProducts(@Field("products") List<ProductInfo> products, @Field("delivery_address") String delivery_address);

}
